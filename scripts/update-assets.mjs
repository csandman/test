import https from 'https';
import { existsSync, createWriteStream, promises as fsPromises } from 'fs';
import rimraf from 'rimraf';
import childProcess from 'child_process';
import { promisify } from 'util';

const exec = promisify(childProcess.exec);

/**
 * Check if a directory exists, and if it does not, create it
 *
 * @param {string} dir The path of the directory to create
 */
async function mkdir(dir) {
  if (!existsSync(dir)) {
    await fsPromises.mkdir(dir, { recursive: true });
  }
}

/**
 * Remove a file or directory
 *
 * @param {string} filePath The source folder/file path
 * @param {object} opts The options to pass to rimraf https://www.npmjs.com/package/rimraf#options
 * @returns {Promise} A promise which throws if the process fails
 */
function rm(filePath, opts = {}) {
  const defaultOpts = { glob: false };

  return new Promise((resolve, reject) => {
    rimraf(filePath, { ...defaultOpts, ...opts }, (err) => {
      if (err) {
        reject(err);
      }
      resolve();
    });
  });
}

const downloadFile = (file) =>
  new Promise((resolve, reject) => {
    rm(file.path).then(() => {
      const writeStream = createWriteStream(file.path);
      https
        .get(file.url, (response) => {
          response.pipe(writeStream);
          writeStream.on('finish', () => {
            console.info(`FINISHED DOWNLOADING FILE: ${file.path}`);
            resolve();
          });
        })
        .on('error', (err) => {
          // Handle errors
          rimraf(file.path); // Delete the file async. (But we don't check the result)
          reject(err);
        });
    });
  });

const downloadStyles = async () => {
  const styles = [
    {
      url:
        'https://raw.githubusercontent.com/file-icons/atom/master/styles/colours.less',
      path: 'tmp/colors.less',
      finalPath: 'src/ts/file-icons/styles/colors.css',
    },
    {
      url:
        'https://raw.githubusercontent.com/file-icons/atom/master/styles/icons.less',
      path: 'tmp/icons.less',
      finalPath: 'src/ts/file-icons/styles/icons.css',
    },
    {
      url:
        'https://raw.githubusercontent.com/file-icons/atom/master/styles/mixins.less',
      path: 'tmp/mixins.less',
    },
  ];

  await mkdir('tmp');

  await Promise.all(styles.map((style) => downloadFile(style)));

  const stylesToCompile = styles.filter((style) => style.finalPath);

  await Promise.all(
    stylesToCompile.map((style) => {
      const cmd = ['lessc', style.path, style.finalPath].join(' ');
      return exec(cmd);
    })
  );

  await rm('tmp');

  console.log('Finished downloading all styles');
};

const downloadDb = async () => {
  const icondb = {
    url:
      'https://raw.githubusercontent.com/file-icons/atom/master/lib/icons/.icondb.js',
    path: 'src/ts/file-icons/icondb.js',
  };

  await downloadFile(icondb);

  console.log('Finished downloading icondb');
};

const downloadFonts = async () => {
  const fonts = [
    {
      url:
        'https://raw.githubusercontent.com/file-icons/vscode/master/icons/devopicons.woff2',
      path: 'src/ts/file-icons/fonts/devopicons.woff2',
    },
    {
      url:
        'https://raw.githubusercontent.com/file-icons/vscode/master/icons/file-icons.woff2',
      path: 'src/ts/file-icons/fonts/file-icons.woff2',
    },
    {
      url:
        'https://raw.githubusercontent.com/file-icons/vscode/master/icons/fontawesome.woff2',
      path: 'src/ts/file-icons/fonts/fontawesome.woff2',
    },
    {
      url:
        'https://raw.githubusercontent.com/file-icons/vscode/master/icons/mfixx.woff2',
      path: 'src/ts/file-icons/fonts/mfixx.woff2',
    },
    {
      url:
        'https://raw.githubusercontent.com/file-icons/vscode/master/icons/octicons.woff2',
      path: 'src/ts/file-icons/fonts/octicons.woff2',
    },
  ];

  await Promise.all(fonts.map((font) => downloadFile(font)));

  console.log('Finished downloading fonts');
};

const updateAssets = async () => {
  await downloadStyles();
  await downloadDb();
  await downloadFonts();
};

updateAssets();
