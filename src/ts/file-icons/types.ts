export type IconColor = [string | null, string | null];

export type IconBase = [
  string,
  IconColor,
  RegExp,
  number?,
  boolean?,
  RegExp?,
  RegExp?,
  RegExp?,
  RegExp?
];

export type IconDbItem = [IconBase[], number[][], Record<string, number>];

export type IconDb = IconDbItem[];
